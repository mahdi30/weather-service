from fastapi import FastAPI
import aiohttp
from app import routers
from contextlib import asynccontextmanager
import pandas as pd

from app.dependencies import get_postcode_data_url

@asynccontextmanager
async def lifespan(application: FastAPI):
    http_client = aiohttp.ClientSession()
    application.state.http_client = http_client
    application.state.postcode_df = await load_postcode_df()
    yield
    await http_client.close()
    application.state.http_client = None

async def load_postcode_df():
    data_url=get_postcode_data_url("ES")
    postcode_df = pd.read_csv(data_url,sep='\t',names=['postcode','lat','long'],usecols=[1,9,10],dtype={1:str})
    postcode_df.set_index("postcode", inplace = True)
    return postcode_df

def create_app():
    app = FastAPI(lifespan=lifespan)
    app.include_router(routers.router, prefix="/v1")
    return app

app = create_app()
