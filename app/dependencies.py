from datetime import datetime,timedelta, timezone

def get_weather_url(longitude: float, latitude : float):
    api_url = "https://www.7timer.info/bin/astro.php?lon={lon}&lat={lat}&ac=0&unit=metric&output=json&tzshift=0"
    return api_url.format(lon=longitude,lat=latitude)

def get_postcode_data_url(country:str):
    data_url = "https://symerio.github.io/postal-codes-data/data/geonames/{country}.txt"
    return data_url.format(country=country)

def get_weather_last_hour(data,hour):
    init_time = datetime.strptime(data['init'],"%Y%m%d%H").replace(tzinfo=timezone.utc)
    result = [map_weather_data(item,init_time) for item in data['dataseries'] if int(item['timepoint'] <= hour)]
    return result

def map_weather_data(item,init_time):
    timepoint = int(item["timepoint"])
    start_period_utc = init_time + timedelta(hours = timepoint-3)
    end_period_utc = init_time + timedelta(hours = timepoint)

    return {
        "start_period_utc":start_period_utc,
        "end_period_utc":end_period_utc,
        "cloud_cover":item["cloudcover"],
        "temperature":item["temp2m"]
    }
