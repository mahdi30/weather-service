from pydantic import BaseModel
from datetime import datetime

class WeatherInfo(BaseModel):
    start_period_utc: datetime
    end_period_utc: datetime
    cloud_cover: int
    temperature: int
