from fastapi import APIRouter, HTTPException, Request
from app.models import WeatherInfo
from app.dependencies import *
import aiohttp

router = APIRouter(
    prefix = "/weather",
    tags = ["weather"]
)



@router.get("/cordinate",response_model=list[WeatherInfo])
async def get_weather_info(longitude: float, latitude : float, request:Request):
    session :aiohttp.ClientSession = request.app.state.http_client
    try:
        async with session.get(get_weather_url(longitude,latitude)) as req:
            response = await req.json(content_type=None)
        result =  get_weather_last_hour(response,48)
    except Exception:
        raise HTTPException(status_code=400, detail="Application Error!")
    return result   

@router.get("/postcode",response_model=list[WeatherInfo])
async def get_weather_postcode(request:Request, postcode:str):
    df = request.app.state.postcode_df
    http_client :aiohttp.ClientSession = request.app.state.http_client 
    if postcode in df.index:
        longitude = df.loc[postcode]['long']
        latitude = df.loc[postcode]['lat']
        try:
            async with http_client.get(get_weather_url(longitude,latitude)) as req:
                response = await req.json(content_type=None)
            result =  get_weather_last_hour(response,48)
        except Exception:
            raise HTTPException(status_code=400,detail="Connectin Error")
    else:
        raise HTTPException(status_code=400,detail="Invalid Postcode")
    return result
