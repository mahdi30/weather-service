# Python Assignment: serving a weather API as a microservice

This implementation provides imperfect weather information using the API of http://www.7timer.info/ through a dockerized microservice.


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/mahdi30/weather-service.git
```

Go to the project directory

```bash
  cd weather-service
```

Build docker image

```bash
  docker build -t weather-service .
```

Run the weather-service container

```bash
  docker run -d -p 8000:8080 weather-service
```

## Sending requests

Get weahter forcast for next 48 hours with latitude and longitude

```
  curl -H "ACCEPT:application/json" http://127.0.0.1:8000/v1/weather/cordinate\?latitude\=5.121\&longitude\=52.091
```

Get weahter forcast for next 48 hours with Spanish Postcode

```
  curl -H "ACCEPT: application/json" http://127.0.0.1:8000/v1/weather/postcode\?postcode\=04006
```

